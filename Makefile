.DEFAULT_GOAL := build

BINARY_NAME=hello-world

build:
	go build -o ${BINARY_NAME} hello.go

compile:
	echo "Compiling for every OS and Platform"
	GOOS=freebsd GOARCH=386 go build -o ${BINARY_NAME}-freebsd-386 hello.go
	GOOS=linux GOARCH=386 go build -o ${BINARY_NAME}-linux-386 hello.go
	GOOS=windows GOARCH=386 go build -o ${BINARY_NAME}-windows-386 hello.go

run:
	go run hello.go

clean:
	go clean
	rm ${BINARY_NAME}
	rm ${BINARY_NAME}-freebsd-386
	rm ${BINARY_NAME}-linux-386
	rm ${BINARY_NAME}-windows-386
